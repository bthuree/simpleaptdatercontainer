FROM debian:bookworm-slim AS debian-addons

MAINTAINER Bengt Thuree "bengt.docker@thuree.com"

ENV TERM xterm
ENV DEBIAN_FRONTEND noninteractive
ENV DEBIAN_PRIORITY critical
ENV DEBCONF_NOWARNINGS yes

ARG uid
ARG user

# RUN run-parts --exit-on-error --verbose /usr/local/debian-base-setup/

# Update
RUN apt-get update 

# Install apt-dater
RUN apt-get -y --no-install-recommends --purge --auto-remove install \
    apt-dater \
    less \
    locales

# Clean up
RUN rm -rf /var/lib/apt/lists/*
RUN apt-get autoremove
RUN apt-get clean -y 
RUN rm -rf /tmp/* 
RUN rm -rf /var/tmp/* 
RUN for logs in `find /var/log -type f`; do > $logs; done 
#RUN rm -rf /usr/share/locale/* 
RUN rm -rf /usr/share/man/* 
RUN rm -rf /usr/share/doc/* 
RUN rm -f /var/cache/apt/*.bin

# Add user
RUN useradd --no-log-init --home-dir /home/$user --create-home --uid $uid --gid users $user 
RUN mkdir /home/$user/.ssh 
RUN mkdir /home/$user/.ssh/cm-files
RUN chown -R $user:users /home/$user/.ssh 
RUN chmod 0700 /home/$user/.ssh 

# Add UTF-8 US for tmux
RUN sed 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' < /etc/locale.gen > /etc/locale.gen2 
RUN mv /etc/locale.gen2 /etc/locale.gen 
RUN locale-gen

VOLUME /home/$user

WORKDIR /home/$user

USER $user

COPY entrypoint.sh ./entrypoint.sh
ENTRYPOINT ["./entrypoint.sh"]
