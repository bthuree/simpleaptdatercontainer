# Simple apt-dater Container

This is a simple container that only does one thing.

Adding the apt-dater feature to an non debian/ubuntu system.

For more information regarding apt-dater, please look at https://github.com/DE-IBH/apt-dater

I used the following container https://github.com/netresearch/docker-apt-dater as a base for my work.

## Assumptions

The following assumptions are made.

1. You can ssh to each of the servers and login as your normal user without any passwords. 
2. You are using your normal ~/.ssh directory for known_hosts, id_rsa, and possible config file.
3. apt-dater-host is installed on each server
4. Your normal user belongs to wheel or adm groups, depending on which system as per apt-dater-host.
5. You have generated an ed25519 key, which has been distributed to all your servers.

## Installation 

1. Clone this repository to a new empty directory.

    git clone git@gitlab.com:bthuree/simpleaptdatercontainer.git

2. Modify docker-compose.yml 

    1. Change the uid to a relevant values for your system. uid should be the same value as you have for your ~/.ssh files.
    2. Change user to a relevant value for your system. user should be the same owner as you have for your ~/.ssh files.

In my case

```code
desktop$ ls -la ~/.ssh/known_hosts
-rw-r--r-- 1 myuser users 8683 jan 30 11:38 /home/myuser/.ssh/known_hosts

desktop$ grep myuser /etc/passwd | awk -F: '{print $3}'
1000

desktop$ egrep "uid|user" docker-compose.yml
    - uid=1000
    - user=myuser
```

3) Create one or more host files (ending with .xml) in the hosts.d directory. The files should have the following syntax.

```xml
    <group name="Alpine Hosts" ssh-user="myuser">
      <host name="host1.example.com"/>
      <host name="host2.example.com"/>
    </group>
    <group name="Debian Hosts" ssh-user="myuser">
      <host name="host3.example.com"/>
      <host name="host4.example.com"/>
    </group>
```

## Run the container

You run the apt-dater container with the following commands.

Ensure that ssh-agent has the correct key

```code
ssh-add ~/.ssh/id_ed25519
```

Then launch the container
```code
docker-compose run --rm apt-dater
```

Or, you can just use the provided script that does that for you
```code
./apt-dater
```