#!/usr/bin/env bash
set -e

if [ `ls -1 ./hosts.d/ | wc -l` == 0 ]; then
    echo ' '
    echo '========================================================================'
    echo 'No config files found in /home/bengt/apt-dater/hosts.d/'
    echo '------------------------------------------------------------------------'
    echo 'You need to provide host config files by binding them into the container'
    echo ' '
else
    cat ~/hosts.d/.hosts.d.xml.head ~/hosts.d/*.xml ~/hosts.d/.hosts.d.xml.foot > ~/hosts.xml
    cat ~/hosts.xml

    # first arg is `-f` or `--some-option` - set apt-dater as entrypoint command and add provided arguments
    if [ "${1#-}" != "$1" ]; then
        set -- apt-dater -c ~/apt-dater.xml "$@"
    fi
    
    # no arguments at all - set apt-dater as entry point command
    if [ $# -eq 0 ]; then
        set -- apt-dater -c ~/apt-dater.xml "$@"
    fi
fi

exec "$@"
